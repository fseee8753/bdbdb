//tomar los valores
const txtNombre = document.getElementById('idNombre').value;
const txtMensaje = document.getElementById('idSalida');
const btnEnviar = document.getElementById('btnEnviar');
const btnLimpiar = document.getElementById('btnLimpiar');
const btnCalcular = document.getElementById('btnCalcular');
const btnLimpiarIMC = document.getElementById('btnLimpiarIMC');

btnEnviar.addEventListener('click', mostrar);
btnCalcular.addEventListener('click', Calcular)

function mostrar() {
    const txtNombre = document.getElementById('idNombre').value;
    const txtMensaje = document.getElementById('idSalida');
    txtMensaje.value = txtNombre;
}

btnLimpiar.addEventListener("click", limpiar)
function limpiar() {

    const txtNombre = document.getElementById('idNombre');
    const txtMensaje = document.getElementById('idSalida');

    txtMensaje.value = '';
    txtNombre.value = '';
}

//PRAACTICA CALCULAR IMC

function Calcular() {
    const txtAltura = document.getElementById('idAltura').value;
    const txtPeso = document.getElementById('idPeso').value;
    let altura = parseFloat(txtAltura);
    let peso = parseFloat(txtPeso)
    let imc = 0;
    imc = peso/(altura* altura);
    document.getElementById('idResultado').value = imc.toFixed(0);
}
btnLimpiarIMC.addEventListener("click", LimpiarIMC)
function LimpiarIMC(){
    const txtAltura = document.getElementById('idAltura');
    const txtPeso = document.getElementById('idPeso');
    txtPeso.value='';
    txtAltura.value='';
    document.getElementById('idResultado').value = '';
    alert("Se limpio correctamente")
}